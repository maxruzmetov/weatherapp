import { combineReducers, createStore } from 'redux';

import { store } from './store';

// Reducers
import { weatherReducer as weather } from '../bus/weather/reducer';

const referenceRootReducer = combineReducers({
  weather,
});

const referenceStore = createStore(referenceRootReducer);

describe('Redux store', () => {
  test('should have a valid state shape', () => {
    expect(store.getState()).toEqual(referenceStore.getState());
  });
});
