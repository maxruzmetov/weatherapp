//Core
import React from 'react';
import { useSelector } from 'react-redux';

//Libs
import moment from 'moment/min/moment-with-locales';

export const CurrentWeather = ({ isFetching }) => {
  const selectedDayInfo = useSelector(
    ({ weather: { weatherData, selectedDay } }) =>
      weatherData.find((day) => day.id === selectedDay) || null
  );

  if (!selectedDayInfo) {
    return null;
  }

  const {
    temperature,
    rain_probability,
    humidity,
    day,
    type,
  } = selectedDayInfo;

  let dayName = moment(day).format('dddd');

  let monthName = moment(day).format('D MMMM');

  let className = `icon ${type}`;

  return (
    <>
      <div className="head">
        <div className={className}></div>
        <div className="current-date">
          <p>{dayName}</p>
          <span>{monthName}</span>
        </div>
      </div>
      <div className="current-weather">
        {isFetching || (
          <>
            <p className="temperature">{temperature}</p>
            <p className="meta">
              <span className="rainy">%{rain_probability}</span>
              <span className="humidity">%{humidity}</span>
            </p>
          </>
        )}
      </div>
    </>
  );
};
