//Core
import React from 'react';

//Libs
import moment from 'moment/min/moment-with-locales';

export const Day = (props) => {
  const { day, type, selectedDay, temperature, id, fn } = props;

  let dayName = moment(day).format('dddd');

  let className = `day ${type}`;

  const setDayCallback = (id, cb) => {
    return () => {
      cb(id);
    };
  };

  if (selectedDay === id) {
    className += ' selected';
  }

  return (
    <div className={className} onClick={setDayCallback(id, fn)}>
      <p>{dayName}</p>
      <span>{temperature}</span>
    </div>
  );
};
