//Core
import React, { useCallback } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

//Components
import { Day } from '../day';

//Actions
import { weatherActions } from '../../actions';

//Hooks
import { useDaysFilter } from '../../hooks/useDaysFilter';

export const Forecast = () => {
  const dispatch = useDispatch();
  const { isFiltered } = useDaysFilter();
  const { selectedDay, weatherData } = useSelector(
    ({ weather: { selectedDay, weatherData } }) => ({
      selectedDay,
      weatherData,
    }),
    shallowEqual
  );

  const setDay = useCallback((id) => dispatch(weatherActions.setDay(id)), [
    dispatch,
  ]);

  const weekForecastJSX = weatherData
    .slice(0, 7)
    .map(({ day, type, temperature, id }) => (
      <Day
        key={id}
        day={day}
        type={type}
        temperature={temperature}
        id={id}
        selectedDay={selectedDay}
        fn={setDay}
      />
    ));

  const messageJSX = weatherData.length === 0 && isFiltered && (
    <p className="message">По заданным критериям нет доступных дней!</p>
  );

  return (
    <div className="forecast">
      {messageJSX}
      {weekForecastJSX}
    </div>
  );
};
