//Core
import React from 'react';
import cx from 'classnames';

//Hooks
import { useDaysFilter } from '../../hooks/useDaysFilter';

export const Filters = () => {
  const {
    type,
    setWeatherType,
    minTemp,
    minTempHandler,
    maxTemp,
    maxTempHandler,
    applyFilters,
    isFiltered,
    isFormBlocked,
    loadDataFromApi,
  } = useDaysFilter();

  const submitHandler = isFiltered
    ? async () => {
        await loadDataFromApi();
        applyFilters();
      }
    : applyFilters;

  return (
    <div className="filter">
      <span
        className={cx('checkbox', {
          selected: type === 'cloudy',
        })}
        data-type="cloudy"
        onClick={setWeatherType}
      >
        Облачно
      </span>
      <span
        className={cx('checkbox', {
          selected: type === 'sunny',
        })}
        data-type="sunny"
        onClick={setWeatherType}
      >
        Солнечно
      </span>
      <p className="custom-input">
        <label htmlFor="min-temperature">Минимальная температура</label>
        <input
          id="min-temperature"
          type="number"
          value={minTemp}
          onChange={minTempHandler}
        />
      </p>
      <p className="custom-input">
        <label htmlFor="min-temperature">Максимальная температура</label>
        <input
          id="max-temperature"
          type="number"
          value={maxTemp}
          onChange={maxTempHandler}
        />
      </p>
      <button onClick={submitHandler} disabled={isFormBlocked}>
        Отфильтровать
      </button>
    </div>
  );
};
