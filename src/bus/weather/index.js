// Core
import React from 'react';

// Styles
import './styles/index.scss';

// Components
import { Forecast } from './components/forecast/';
import { CurrentWeather } from './components/current/';
import { Filters } from './components/filter/';
import { Loader } from '../../components/loader';

//Hooks
import { useWeatherFetch } from './hooks/useWeatherFetch';

export const Weather = () => {
  const { isFetching } = useWeatherFetch();

  return (
    <main>
      <Filters />
      <CurrentWeather isFetching={isFetching} />
      <Forecast />

      {isFetching && <Loader />}
    </main>
  );
};
