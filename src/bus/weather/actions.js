//Helper
import { nanoid } from 'nanoid';

//Types
import { types } from './types';

//Api
import { api } from '../../api';

export const weatherActions = Object.freeze({
  //Sync
  startFetching: () => {
    return {
      type: types.WEATHER_START_FETCHING,
    };
  },
  stopFetching: () => {
    return {
      type: types.WEATHER_STOP_FETCHING,
    };
  },
  fill: (payload) => {
    return {
      type: types.WEATHER_FILL,
      payload,
    };
  },
  setFetchingError: (error) => {
    return {
      type: types.WEATHER_SET_FETCHING_ERROR,
      payload: error,
      error: true,
    };
  },
  setDay: (payload) => {
    return {
      type: types.WEATHER_SET_DAY,
      payload,
    };
  },
  filterDays: (payload) => {
    return {
      type: types.WEATHER_FILTER_DAYS,
      payload,
    };
  },
  //Async
  fetchAsync: () => async (dispatch) => {
    dispatch({
      type: types.WEATHER_FETCH_ASYNC,
    });

    dispatch(weatherActions.startFetching());

    const response = await api.weather.fetch();

    if (response.status === 200) {
      const { data } = await response.json();

      const markedData = data.map(({ ...values }) => ({
        ...values,
        id: nanoid(4),
      }));

      dispatch(weatherActions.fill(markedData));
    } else {
      const error = {
        status: response.error,
      };
      dispatch(weatherActions.setFetchingError(error));
    }
    dispatch(weatherActions.stopFetching());
  },
});
