import { types } from './types';

const initialState = {
  weatherData: [],
  isFetching: false,
  error: false,
  selectedDay: null,
  isFiltered: false,
};

export const weatherReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case types.WEATHER_START_FETCHING:
      return {
        ...state,
        isFetching: true,
      };
    case types.WEATHER_FILL:
      return {
        ...state,
        weatherData: payload,
        selectedDay: payload[0]?.id,
        isFetching: false,
        isFiltered: false,
      };
    case types.WEATHER_SET_DAY:
      return {
        ...state,
        selectedDay: payload,
      };
    case types.WEATHER_FILTER_DAYS:
      const filter = payload;

      const filteredWeatherData = state.weatherData.filter((day) => {
        const isType = filter.type ? filter.type === day.type : true;
        const isMinTemp = filter.minTemp
          ? filter.minTemp <= day.temperature
          : true;
        const isMaxTemp = filter.maxTemp
          ? filter.maxTemp >= day.temperature
          : true;

        return isType && isMinTemp && isMaxTemp;
      });

      return {
        ...state,
        weatherData: filteredWeatherData,
        selectedDay: filteredWeatherData[0]?.id,
        isFiltered: true,
      };
    case types.WEATHER_SET_FETCHING_ERROR:
      return {
        ...state,
        error: payload,
        isFetching: false,
      };

    default:
      return state;
  }
};
