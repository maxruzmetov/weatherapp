//Core
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

//Actions
import { weatherActions } from '../actions';

export const useWeatherFetch = () => {
  const dispatch = useDispatch();

  const { weatherData, isFetching, error, selectedDay } = useSelector(
    (state) => state.weather
  );

  useEffect(() => {
    dispatch(weatherActions.fetchAsync());
  }, [dispatch]);

  return {
    weatherData,
    isFetching,
    error,
    selectedDay,
  };
};
