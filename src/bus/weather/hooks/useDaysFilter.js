// Core
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Actions
import { weatherActions } from '../actions';

export const useDaysFilter = () => {
  const dispatch = useDispatch();
  const { isFiltered } = useSelector((state) => state.weather);

  const [type, setType] = useState('');
  const [isFormBlocked, setIsFormBlocked] = useState(true);
  const [minTemp, setMinTemp] = useState('');
  const [maxTemp, setMaxTemp] = useState('');

  const loadDataFromApi = async () => {
    await dispatch(weatherActions.fetchAsync());
  };

  const setWeatherType = ({ target }) => {
    setType(target.dataset.type === type ? '' : target.dataset.type);
    setIsFormBlocked(false);
  };

  const minTempHandler = (event) => {
    setMinTemp(event.target.value);
    setIsFormBlocked(false);
  };

  const maxTempHandler = (event) => {
    setMaxTemp(event.target.value);
    setIsFormBlocked(false);
  };

  const applyFilters = () => {
    const filters = {
      type,
      minTemp,
      maxTemp,
    };
    dispatch(weatherActions.filterDays(filters));

    setIsFormBlocked(true);
  };

  return {
    type,
    setWeatherType,
    minTemp,
    minTempHandler,
    maxTemp,
    maxTempHandler,
    applyFilters,
    isFormBlocked,
    isFiltered,
    loadDataFromApi,
  };
};
